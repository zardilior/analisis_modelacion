#include "printers.h"
#include "Singleton.h"
#include "Documents.h"
class Main: public Singleton<Main>
{
    public:
        Documents ** d;
        printers ** p;
        void printAllDocuments(Documents ** d, int countDocs, printers** p,int countPrinters);
};
