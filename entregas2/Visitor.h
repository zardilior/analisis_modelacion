#ifndef Visitor_h
#define Visitor_h
#include "Singleton.h"
#include "Documents.h"
class Visitor:public Singleton<Visitor>{
    public:
        virtual void visit(Documents * d);       

};
#endif
