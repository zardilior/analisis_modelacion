#ifndef Singleton_h
#define Singleton_h
template <typename T>
class Singleton{
   private: 
       static T * instance;
    public:
        static T * getInstance();
};
#endif
