#include <iostream>
#include <string>	
using namespace std;
#ifndef Singleton_h
#define Singleton_h
template <typename T>
class Singleton{
 
    public:
    	static T * instance;
        static T * getInstance();
};
#endif

template <typename T>
T * Singleton<T>::instance = 0;

template <typename T>
T * Singleton<T>::getInstance(){
    if(instance==0){
        instance = new T();
    }
    return instance;
}
class printers;
#ifndef Documents_h
#define Documents_h
class Documents{
    public:
    	virtual string getName();
        void printOn (printers* p);
};
string Documents::getName (){
	return "";
}
#endif

class Visitor:public Singleton<Visitor>{
    public:
        virtual void visit(Documents * d);       

};
void Visitor::visit(Documents * d){

}

#ifndef printers_h 
#define printers_h 
class printers:public Visitor{
    public:
    	virtual string getName();
        void visit(Documents * d);
};
#endif
string printers::getName (){
	return "";
}
void printers::visit(Documents * d){

    cout<<"Se imprimira el documento de tipo "<<d->getName()<<endl;
    cout<<"En la impresora  de tipo "<<this->getName()<<endl;
}




void Documents::printOn(printers* p){
    p->visit(this);
}

class Main: public Singleton<Main>
{
    public:
        Documents ** d;
        printers ** p;
        void printAllDocuments(Documents ** d, int countDocs, printers** p,int countPrinters);
};

void Main::printAllDocuments(Documents ** d, int countDocs, printers** p,int countPrinters)
{
    for(int i=0; i< countDocs; i++){
        for(int j=0;j< countPrinters; j++){
            d[i]->printOn(p[i]);
        }
    }
}

class impresoraPortatil: public printers{
	string getName();
};
class tarjetaDePresentacion:public Documents{
	string getName();
};

string impresoraPortatil::getName(){
	return "ImpresoraPortatil";
}
string 	tarjetaDePresentacion::getName(){
	return "tarjetaDePresentacion";		
}


int main(){
   Main* x= Main::getInstance();
    int a = 1;
    x->d = new Documents *[a];     
    x->p = new printers *[a];     
    x->d[0] = new tarjetaDePresentacion();
    x->p[0] = new impresoraPortatil();
    x->printAllDocuments(x->d,a,x->p,a);
   return 0;
}


