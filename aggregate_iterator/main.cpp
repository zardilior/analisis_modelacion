#include<iostream>
using namespace std;

template<class T>
class Iterator;

template<class T>
class Aggregate // Contenedor de elementos
{
protected:
    T* list;
    int size;
    int numElems;
public:
    friend Iterator<T>;
    Aggregate(int size = 10)
    {
        this->size = size;
        numElems = 0;
        list = new T[size];
    }
    void add(T value)
    {
        if(numElems<size)
        {
            list[numElems++] = value;
        }
    }
    Iterator<T>* createIterator();
};

template<class T>
class Iterator
{
protected:
    Aggregate<T>& container;
    int idx; //índice o posición en que se encuentra el iterador  
public:
    //importante asignar container en lista inicialización
    Iterator(Aggregate<T>& container)
    :container(container), idx(0){}
    
    T first()
    {
        idx = 0;
        return container.list[0];
    }
};

template<class T>
Iterator<T>* Aggregate<T>::createIterator()
{
    return new Iterator<T>(*this);
}

int main()
{
    Aggregate<int> a(5);
    for(int i=0; i<5; i++)
        a.add(i+1);
    
    Iterator<int>* it = a.createIterator();
    
    cout << it->first() << endl;
}

